package main

import (
	"promo/models"
	"github.com/byorty/beego/orm"
)

func main() {
	models.Init()
	orm.RunCommand()
}

