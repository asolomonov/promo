package main

import (
	"promo/models"
	"promo/util"
	"github.com/byorty/beego"
	"fmt"
)

func main() {
	models.Init()
	email := beego.AppConfig.String("admin.email")
	user := models.NewUser().ByEmail(email)
	password := util.CreatePassword()
	user.Password = util.GetHashedPassword(password)
	if user.Id == models.INVALID_ID {
		user.RoleId = models.USER_ROLE_ADMIN
		user.StatusId = models.USER_STATUS_ACTIVE
		models.GetOrm().Insert(user)
	} else {
		models.GetOrm().Update(user)
	}
	fmt.Println("Администратор")
	fmt.Printf("Логин:  %s\n", email)
	fmt.Printf("Пароль: %s\n", password)
}

