package main

import (
	"promo/models"
	"io/ioutil"
	"encoding/xml"
	"github.com/byorty/beego"
	"strings"
	"time"
	"github.com/byorty/beego/orm"
	"promo/util"
	"net/url"
	"net/http"
	"os"
)

const (
	TIME_LAYOUT = "2006-01-02 15:04:05"
)

func main() {
	models.Init()
	resp, err := http.Get(beego.AppConfig.String("ap.xml"))
	if err != nil {
		beego.Critical(err)
		os.Exit(1)
	} else {
		defer resp.Body.Close()
		bytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			beego.Critical(err)
			os.Exit(1)
		}
		var promotions models.Promotions
		err = xml.Unmarshal(bytes, &promotions)
		if err == nil {
			models.GetOrm().Begin()
			offersWithCategories := make(map[int]map[int]*models.OfferCategory)
			models.GetQuery(models.NewOfferCategory()).
				Update(orm.Params {
					"status_id": models.OFFER_CATEGORY_INACTIVE,
				})
			models.GetQuery(models.NewOffer()).
				Update(orm.Params {
					"status_id": models.OFFER_INACTIVE,
				})
			models.GetQuery(models.NewCoupon()).
				Update(orm.Params {
					"status_id": models.COUPON_INACTIVE,
				})
			for _, promotion := range promotions.Items {
				offer := models.NewOffer().ByExternalId(promotion.OfferId)
				offer.Logo = promotion.Logo
				offer.ExternalId = promotion.OfferId
				offer.Link = promotion.OfferLink
				offer.StatusId = models.OFFER_ACTIVE
				if offer.Id == models.INVALID_ID {
					offerUrl, _ := url.Parse(offer.Link)
					offerUrlParts := strings.Split(offerUrl.Host, ".")
					if offerUrlParts[0] == "www" {
						offerUrlParts = offerUrlParts[1:]
					}
					offer.Path = strings.Join(offerUrlParts, "-")
					offer.Name = promotion.OfferName
					models.GetOrm().Insert(offer)
				} else {
					models.GetOrm().Update(offer)
				}

				if _, ok := offersWithCategories[offer.Id]; !ok {
					offersWithCategories[offer.Id] = make(map[int]*models.OfferCategory)
				}

				for _, name := range strings.Split(promotion.Categories, util.COMMA) {
					name = strings.TrimSpace(name)
					path := util.ToSeoPath(name)
					category := models.NewOfferCategory().ByPath(path)
					category.StatusId = models.OFFER_CATEGORY_ACTIVE
					if category.Id == models.INVALID_ID {
						category.Path = path
						category.Name = name
						models.GetOrm().Insert(category)
					} else {
						models.GetOrm().Update(category)
					}
					if _, ok := offersWithCategories[offer.Id][category.Id]; !ok {
						offersWithCategories[offer.Id][category.Id] = category
					}
				}

				coupon := models.NewCoupon().ByExternalId(promotion.Id)
				coupon.ExternalId = promotion.Id
				coupon.Offer = offer
				coupon.Code = promotion.Code
				coupon.Name = promotion.Title
				coupon.Description = promotion.Description
				coupon.BeginDate, _ = time.Parse(TIME_LAYOUT, promotion.BeginDate)
				coupon.EndDate, _ = time.Parse(TIME_LAYOUT, promotion.EndDate)
				for id, name := range models.NewCouponExternalStatus(models.COUPON_EXTERNAL_DISABLED).GetExternalNames() {
					if promotion.Status == name {
						coupon.ExternalStatusId = id
					}
				}
				coupon.Link = promotion.Link
				if promotion.Exclusive == "Да" {
					coupon.Exclusive = true
				} else {
					coupon.Exclusive = false
				}
				coupon.Rating = promotion.Rating
				coupon.TypeId = models.NewCouponType(promotion.TypeId - 1).Id
				coupon.StatusId = models.COUPON_ACTIVE
				if coupon.Id == models.INVALID_ID {
					models.GetOrm().Insert(coupon)
					models.GetQuery(coupon).
						Filter("Offer", offer.Id).
						Update(orm.Params {
							"rank": orm.ColValue(orm.Col_Add, 1),
						})
				} else {
					models.GetOrm().Update(coupon)
				}
			}

			for offerId, categoryMap := range offersWithCategories {
				offer := models.NewOffer().ById(offerId)
				if offer.Id > 0 {
					m2m := models.GetOrm().QueryM2M(offer, "Categories")
					if m2m.Exist(offer) {
						m2m.Remove(offer.Categories)
					}
					if offer.StatusId != models.OFFER_INACTIVE {
						for _, category := range categoryMap {
							m2m.Add(category)
						}
					}
				}
			}
			models.GetOrm().Commit()
		} else {
			beego.Critical(err)
		}
	}
}
