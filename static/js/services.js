app
    .factory('Search', ['$resource', function($resource) {
        return $resource('/');
    }])
    .factory('Dashboard', ['$resource', function($resource) {
        return $resource('/api/admin/dashboard');
    }])
    .factory('Coupon', ['$resource', function($resource) {
        return $resource('/api/admin/coupons');
    }])
    .factory('UserCoupon', ['$resource', function($resource) {
        return $resource('/api/coupons/:key/:value');
    }])
    .factory('Offer', ['$resource', function($resource) {
        return $resource('/api/admin/offers');
    }])
    .factory('Observer', ['$rootScope', function($rootScope) {
        var params = {};
        var handlers = {};
        var destructors = [];
        var updateParams = function(name, data) {
            if (angular.isObject(data)) {
                angular.forEach(data, function(value, key) {
                    params[key] = value;
                });
            } else {
                params[name] = data;
            }
        };
        var timeout = null;
        var notify = function(type, data, callback, force) {
            force = force || false;
            if (type) {
                updateParams(type, data);
            }
            var call = function() {
                callback({
                    type: type,
                    data: params
                });
            };
            if (force) {
                call();
            } else {
                if (timeout) {
                    clearTimeout(timeout);
                    timeout = null;
                }
                timeout = setTimeout(call, 0);
            }
        };
        var trim = function(str) {
            return str.replace(/^\s+|\s+$/g, '');
        };
        $rootScope.$on('$routeChangeStart', function (event, next, current) {
            params = {};
            handlers = {};
            angular.forEach(destructors, function(destructor) {
                destructor();
            });
            destructors = [];
        });
        return {
            on: function(events, handler) {
                angular.forEach(events.split(','), function(event) {
                    event = trim(event);
                    destructors.push(
                        $rootScope.$on(event, function(_, data) {
                            notify(event, data, handler);
                        })
                    );
                });
                return this;
            },
            watch: function(trackings, handler) {
                angular.forEach(trackings.split(','), function(tracking) {
                    tracking = trim(tracking);
                    destructors.push(
                        $rootScope.$$childTail.$watch(tracking, function(newData, oldData) {
                            if (newData != oldData) {
                                notify(tracking, newData, handler);
                            }
                        })
                    );
                });
                return this;
            },
            custom: function(customs, handler) {
                angular.forEach(customs.split(','), function(custom) {
                    custom = trim(custom);
                    handlers[custom] = handler;
                    notify(custom, null, handlers[custom]);
                });
                return this;
            },
            notify: function(type, data) {
                data = data || null;
                var handler = handlers[type];
                if (angular.isFunction(handler)) {
                    notify(type, data, handler, true);
                } else {
                    $rootScope.$emit(type, data);
                }
                return this;
            }
        }
    }])
    .factory('AttrForm', function() {

        var Form = function(scope) {
            this.scope = scope;
            this.rules = {};
        };

        Form.prototype.add = function(name, defaultValue, require) {
            this.rules[name] = {
                defaultValue: defaultValue || undefined,
                require: require
            };
            return this;
        };

        Form.prototype.require = function(name, defaultValue) {
            return this.add(name, defaultValue, true);
        };

        Form.prototype.optional = function(name, defaultValue) {
            return this.add(name, defaultValue, false);
        };

        Form.prototype.check = function(attrs) {
            for (var name in this.rules) {
                var rule = this.rules[name];
                this.scope[name] = angular.isDefined(attrs[name]) ? attrs[name] : rule.defaultValue;
                if (rule.require && angular.isUndefined(this.scope[name])) {
                    throw new Error('attribute ' + name + ' is undefined');
                }
            }
        };

        return {
            init: function(scope) {
                return new Form(scope);
            }
        };
    })
;