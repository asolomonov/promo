function getService(str) {
    var serviceParts = str.split('.');
    var method = serviceParts.length == 2 ? serviceParts[1] : 'save';
    var injector = angular.element(document).injector();
    return injector.get(serviceParts[0])[method];
}

angular.element.prototype.getStyle = function(prop) {
    if (!this.length) {
        return null;
    }
    var element = this[0];
    if (element.currentStyle) {
        return element.currentStyle[prop] || null;
    } else if (window.getComputedStyle) {
        prop = prop.replace(/[A-Z]/, function(char) {
            return '-' + char.toLowerCase();
        });
        return window.getComputedStyle(element).getPropertyValue(prop) || null;
    }
    return null;
};

angular.element.prototype.getOffset = function() {
    var top = 0;
    var left = 0;
    if (this.length) {
        var elem = this[0];
        if (elem.getBoundingClientRect) {
            var box = elem.getBoundingClientRect();
            var body = document.body;
            var docElem = document.documentElement;
            var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
            var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;
            var clientTop = docElem.clientTop || body.clientTop || 0;
            var clientLeft = docElem.clientLeft || body.clientLeft || 0;
            top  = box.top +  scrollTop - clientTop;
            left = box.left + scrollLeft - clientLeft;
        } else {
            while(elem) {
                top = top + parseInt(elem.offsetTop);
                left = left + parseInt(elem.offsetLeft);
                elem = elem.offsetParent;
            }
        }
    }
    return {
        top: top,
        left: left
    };
};

app
    .directive('ajaxForm', ['$http', '$log', 'AttrForm', function($http, $log, form) {
        return {
            link: function(scope, element, attrs) {
                form
                    .init(scope)
                    .require('service')
                    .optional('method', 'save')
                    .check(attrs)
                ;

                element.bind('submit', function(event) {
                    event.preventDefault();
                    var data = {};
                    var inputsMap = {};
                    var domElement = element[0];
                    var errorWrapper = angular.element(domElement.querySelector('.form-error-wrapper'));
                    if (errorWrapper.length) {
                        errorWrapper.empty();
                    }
                    var inputs = element.length ? domElement.elements : [];
                    var submit = null;
                    for (var i in inputs) {
                        if (angular.isObject(inputs[i])) {
                            var input = angular.element(inputs[i]);
                            if (input && input.attr('name') && input.val()) {
                                data[input.attr('name')] = input.val();
                                inputsMap[input.attr('name')] = input;
                                input.parent().removeClass('has-error');
                            } else if('submit' == input.attr('type')) {
                                submit = input.addClass('disabled');
                            }
                        }
                    }

                    getService(scope.service)(null, data, function(resp) {
                        submit.removeClass('disabled');
                        if (resp.success) {
                            scope.$emit('formSuccess', resp);
                            for (var i in inputs) {
                                if (angular.isObject(inputs[i])) {
                                    var input = angular.element(inputs[i]);
                                    if (input && input.attr('type') == 'text' && input.val()) {
                                        input.val('');
                                    }
                                }
                            }
                        } else if (resp.error) {
                            if (angular.isObject(resp.messages)) {
                                angular.forEach(resp.messages, function(message, key) {
                                    if (inputsMap[key]) {
                                        inputsMap[key].parent().addClass('has-error');
                                        var div = angular.element(document.createElement('div'));
                                        div.html(message);
                                        errorWrapper.append(div);
                                    }
                                });
                            } else if (angular.isString(resp.message)) {
                                if (errorWrapper.length) {
                                    errorWrapper.html(resp.message);
                                }
                            }
                            scope.$emit('formError', resp);
                        }
                    });
                });
            }
        }
    }])
    .directive('infiniteScroll', ['$rootScope', '$window', 'AttrForm', function ($rootScope, $window, form) {
        return {
            scope: true,
            link: function (scope, elem, attrs) {
                form
                    .init(scope)
                    .optional('param', 'page')
                    .optional('page', 1)
                    .optional('totalPages', 0)
                    .optional('emitOnInit', 'true')
                    .check(attrs)
                ;
                scope.page = parseInt(scope.page);
                scope.totalPages = parseInt(scope.totalPages);
                scope.emitOnInit = scope.emitOnInit == 'true';

                var shouldEmit = true;
                var emitPages = {};
                var emitPage = function(page) {
                    if (!emitPages[page]) {
                        scope.$emit(scope.param, page);
                        emitPages[page] = true;
                    }
                    markPage(page);
                };
                var markPage = function(page) {
                    emitPages[page] = true;
                };
                $window = angular.element($window);
                $window.bind('scroll', function () {
                    var windowHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
                    var scrollTop = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop || 0;
                    var elementBottom = elem.getOffset().top + parseInt(elem.getStyle('height'));
                    var windowBottom = windowHeight + scrollTop;
                    var emitPrevPage = scrollTop <= 0 && scope.page > 1;
                    var emitNextPage = scope.page < scope.totalPages && elementBottom - windowBottom <= windowHeight;
                    if ((emitPrevPage || emitNextPage) && shouldEmit) {
                        shouldEmit = false;
                        var page = emitPrevPage ? scope.page - 1 : scope.page + 1;
                        emitPage(page);
                    }
                });

                scope.$on('paginator', function(event, args) {
                    if (angular.isObject(args)) {
                        shouldEmit = true;
                        scope.page = args.page;
                        scope.totalPages = args.totalPages;
                    }
                });

                scope.emitOnInit ? emitPage(scope.page) : markPage(scope.page);
            }
        };
    }])
    .directive('coupons', function() {
        return {
            link: function(scope, element, attrs) {
                var page = scope.page;
                scope.$on('coupons', function(event, resp) {
                    var currentPage = resp.paginator.page;
                    var html = element.html();
                    element.html(
                        currentPage > page ? html + resp.content : resp.content + html
                    );
                    page = currentPage;
                });
            }
        }
    })
;

