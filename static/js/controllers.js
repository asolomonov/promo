app
    .controller('DropdownCtrl', ['$scope', function($scope) {
        $scope.items = [];
        $scope.name = null;
        $scope.value = null;
        $scope.isOpen = false;

        $scope.select = function(id) {
            for (var i in $scope.items) {
                if ($scope.items[i].id == id) {
                    $scope.items[i].selected = true;
                    $scope.name = $scope.items[i].name;
                    $scope.value = $scope.items[i].id;
                } else {
                    $scope.items[i].selected = false;
                }
            }
            $scope.isOpen = false;
        };

        $scope.isShow = function() {
            return 0 < $scope.items.length;
        }

        $scope.$watch('listen', function() {
            $scope.$on($scope.listen, function(event, items) {
                $scope.items = items;
                if ($scope.value == null && $scope.items.length) {
                    $scope.value = $scope.items[0].id;
                }
                $scope.select($scope.value);
            });
        });
    }])
    .controller('PaginationCtrl', ['$scope', '$location', function($scope, $location) {
        $scope.page = 1;
        $scope.totalItems = 0;
        $scope.itemsPerPage = 0;

        $scope.$on('paginator', function(event, args) {
            if (angular.isObject(args)) {
                $scope.page = args.page;
                $scope.totalItems = args.totalItems;
                $scope.totalPages = args.totalPages;
                $scope.itemsPerPage = args.itemsPerPage;
            }
        });

        $scope.isShow = function() {
            return $scope.totalPages > 1;
        };

        var getPageFromLocation = function() {
            var page = parseInt($location.search()[$scope.searchParam]);
            if (angular.isNumber(page) && page >= 1) {
                return page;
            } else {
                return null;
            }
        };

        $scope.pageChanged = function() {
            $location.search($scope.searchParam, $scope.page);
            $scope.$emit($scope.emit, $scope.page);
        };

        var emit = function() {
            var page = getPageFromLocation() || 1;
            if (page) {
                $scope.$emit($scope.emit, page);
            }
        };

        $scope.$watch('emit', function() {
            emit();
        });
    }])
    .controller('RadioCtrl', ['$scope', function($scope) {
        $scope.listen = $scope.listen || 'radio';
        $scope.selected = angular.isDefined($scope.selected) ? $scope.selected : 1;
        $scope.select = function(id) {
            $scope.selected = id;
        };
        $scope.items = [];

        $scope.$watch('listen', function() {
            $scope.$on($scope.listen, function(event, items) {
                $scope.items = items
            });
        });

        $scope.$on($scope.listen, function(event, items) {
            $scope.items = items
        });
    }])
    .controller('HomeCtrl', ['$location', '$rootScope', '$window', 'Observer', 'UserCoupon', function($location, $rootScope, $window, Observer, UserCoupon) {
        $location.search('page', null);
        Observer
            .on('page', function(event) {
                var params = $location.search();
                for (var i in params) {
                    $window.pathParams[i] = params[i];
                }
                $window.pathParams['page'] = event.data.page;

                UserCoupon.get($window.pathParams, function(resp) {
                    $rootScope.$broadcast('coupons', resp);
                });
            })
        ;
    }])
    .controller('RootCtrl', ['$scope', function($scope) {
        var
            SHOW_CATEGORIES = 1,
            SHOW_SHOPS      = 2,
            HIDE_FILTER     = 0,
            OPEN_FILTER     = 1,
            CLOSE_FILTER    = 2
        ;
        var type = null;
        var showFilter = HIDE_FILTER;
        $scope.shop = SHOW_SHOPS;
        $scope.category = SHOW_CATEGORIES;
        $scope.toggle = function(newType) {
            if ($scope.isOpening() && type != newType) {
                type = newType;
                return;
            } else {
                type = newType;
            }
            switch (showFilter) {
                case HIDE_FILTER:
                case CLOSE_FILTER:
                    showFilter = OPEN_FILTER;
                    break;
                case OPEN_FILTER:
                    showFilter = CLOSE_FILTER;
                    break;
                case CLOSE_FILTER:
                    showFilter = OPEN_FILTER;
                    break;
            }
        };
        $scope.isOpening = function() {
            return showFilter == OPEN_FILTER;
        };
        $scope.showCategories = function() {
            return type == SHOW_CATEGORIES;
        };
        $scope.showShops = function() {
            return type == SHOW_SHOPS;
        };
    }])
    .controller('DashboardCtrl', ['$scope', 'Dashboard', function($scope, Dashboard) {
        $scope.categories = 0;
        $scope.offers = 0;
        $scope.coupons = 0;
        Dashboard.get(function(resp) {
            $scope.categories = resp.categories;
            $scope.offers = resp.offers;
            $scope.coupons = resp.coupons;
        })
    }])
    .controller('CouponsCtrl', ['$scope', '$location', 'Observer', 'Coupon', function($scope, $location, Observer, Coupon) {
        $scope.items = null;
        Observer
            .on('page',function(event) {
                Coupon.get(event.data, function(resp) {
                    if (resp.success) {
                        $scope.items = resp.items;
                    }
                });
            })
        ;
    }])
    .controller('CouponCtrl', ['$scope', '$upload', '$location', '$timeout', function($scope, $upload, $location, $timeout) {
        var upload = null;
        $scope.onFileSelect = function($files) {
            for (var i = 0; i < $files.length; i++) {
                upload = $upload
                    .upload({
                        url: '/api/admin/coupons?id=' + $location.search()['id'],
                        file: $files[i]
                    })
                    .success(function(data, status, headers, config) {
                        $timeout(function() {
                            $location.path('/admin/coupons');
                        }, 1);
                    })
                ;
            }
        };
    }])
    .controller('OffersCtrl', ['$scope', '$modal', 'Offer', 'Observer', function($scope, $modal, Offer, Observer) {
        $scope.items = null;
        Observer
            .on('page',function(event) {
                Offer.get(event.data, function(resp) {
                    if (resp.success) {
                        $scope.items = resp.items;
                    }
                });
            })
        ;
    }])
    .controller('OfferCtrl', ['$scope', '$location', '$timeout', 'Observer', 'Offer', function($scope, $location, $timeout, Observer, Offer) {
        $scope.item = null;
        Offer.get({id: $location.search()['id']}, function(resp) {
            if (resp.success) {
                $scope.item = resp.item;
            }
        });
        Observer
            .on('formSuccess', function(resp) {
                if (resp.data.success) {
                    $timeout(function() {
                        $location.path('/admin/offers');
                    }, 1);
                }
            })
        ;
    }])
;