package routers

import (
	"promo/controllers"
	"github.com/byorty/beego"
	"promo/util"
)

func init() {
    beego.Router(util.URL_ROOT, &controllers.HomeController{})
    beego.Router(`/category/:category([a-z0-9\-]+)`, &controllers.HomeController{})
    beego.Router(`/shop/:shop([a-z0-9\-]+)/`, &controllers.HomeController{})
    beego.Router(`/coupon/:id([0-9]+)/`, &controllers.CouponController{})
    beego.Router(`/api/coupons`, &controllers.ApiCoupons{})
    beego.Router(`/api/coupons/category/:category([a-z0-9\-]+)`, &controllers.ApiCoupons{})
    beego.Router(`/api/coupons/shop/:shop([a-z0-9\-]+)/`, &controllers.ApiCoupons{})
//    beego.Router(util.URL_SINGIN, &controllers.SigninController{})
//    beego.Router(util.URL_SINGOUT, &controllers.SignoutController{})
//    beego.Router(`/admin/*`, &controllers.AdminController{})
//    beego.Router(`/api/admin/dashboard`, &controllers.ApiAdminDashboardController{})
//    beego.Router(`/api/admin/coupons`, &controllers.ApiAdminCouponsController{})
//    beego.Router(`/api/admin/offers`, &controllers.ApiAdminOffersController{})
}
