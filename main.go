package main

import (
	"github.com/byorty/beego"
	"promo/models"
	_ "promo/routers"
	_ "promo/filters"
)

func main() {
	models.Init()
	beego.Run()
}

