package models

import (
	"time"
	"promo/models/enum"
)

type Offer struct {
	Id           int              `orm:"pk;auto" json:"id"`
	ExternalId   int              `json:"-"`
	Name         string           `json:"name"`
	ShortName    string           `json:"shortName"`
	Logo         string           `json:"logo"`
	Link         string           `json:"link"`
	CreateDate   time.Time        `orm:"auto_now_add;type(datetime)" json:"-"`
	StatusId     int              `json:"statusId"`
	Path         string           `json:"-"`
	Coupons      []*Coupon        `orm:"reverse(many)" json:"-"`
	Categories   []*OfferCategory `orm:"rel(m2m)" json:"-"`
}

func NewOffer() *Offer {
	return new(Offer)
}

func (this *Offer) GetName() string {
	return this.Name
}

func (this *Offer) ById(id int) *Offer {
	this.Id = id
	GetOrm().Read(this)
	return this
}

func (this *Offer) ByExternalId(id int) *Offer {
	this.ExternalId = id
	GetOrm().Read(this, "ExternalId")
	return this
}

func (this *Offer) ByPath(path string) *Offer {
	this.Path = path
	GetOrm().Read(this, "Path")
	return this
}

func (this *Offer) All() []*Offer {
	var offers []*Offer
	query := GetQuery(this)
	query.All(&offers)
	return offers
}

func (this *Offer) AllActive() []*Offer {
	var offers []*Offer
	query := GetQuery(this)
	query.
		Filter("StatusId", OFFER_ACTIVE).
		OrderBy("Name").
		All(&offers)
	return offers
}

type Offers []*Offer

func (slice Offers) Len() int {
	return len(slice)
}

func (slice Offers) Less(i, j int) bool {
	return slice[i].Name < slice[j].Name;
}

func (slice Offers) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

const (
	OFFER_ADDED    int = iota
	OFFER_ACTIVE
	OFFER_INACTIVE
)

var offerStatusNames = enum.Map {
	OFFER_ADDED   : "Добавленный",
	OFFER_ACTIVE  : "Активный",
	OFFER_INACTIVE: "Неактивный",
}

var offerStatusEnums = enum.List {
	OFFER_ADDED   : OfferStatusAdded(),
	OFFER_ACTIVE  : OfferStatusActive(),
	OFFER_INACTIVE: OfferStatusInactive(),
}

type OfferStatus struct {
	*enum.BaseEnum
}

func (this *OfferStatus) GetNames() enum.Map {
	return offerStatusNames
}

func (this *OfferStatus) GetList() enum.List {
	return offerStatusEnums
}

func NewOfferStatus(id int) *OfferStatus {
	status := new(OfferStatus)
	status.BaseEnum, _ = enum.Create(status, id)
	return status
}

func OfferStatusAdded() *OfferStatus {
	return NewOfferStatus(OFFER_ADDED)
}

func OfferStatusActive() *OfferStatus {
	return NewOfferStatus(OFFER_ADDED)
}

func OfferStatusInactive() *OfferStatus {
	return NewOfferStatus(OFFER_ADDED)
}
