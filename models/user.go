package models

import (
	"time"
	"promo/models/enum"
	"promo/util"
	"fmt"
)

type User struct {
	Id           int       `orm:"pk;auto"`
	Email        string    `orm:"size(100)"`
	Password     string    `orm:"size(32)"`
	RoleId       int
	StatusId     int
	RegisterDate time.Time `orm:"auto_now_add;type(datetime)"`
	LoginIp      string    `orm:"type(text);null"`
	LoginDate    string    `orm:"auto_now;type(datetime)"`
	UserAgent    string    `orm:"type(text);null"`
}

func NewUser() *User {
	user := new(User)
	return user
}

func (this *User) ByEmail(email string) *User {
	this.Email = email
	GetOrm().Read(this, "Email")
	return this
}

func (this *User) IsGuest() bool {
	return this.RoleId == USER_ROLE_GUEST
}

func (this *User) IsLoggedUser() bool {
	return this.RoleId >= USER_ROLE_USER
}

func (this *User) IsLoggedAdmin() bool {
	return this.RoleId == USER_ROLE_ADMIN
}

func (this *User) GetAngularClientHash() string {
	return util.Md5(fmt.Sprintf("%d,%s,%s", this.Id, this.LoginIp, this.UserAgent))
}

const (
	USER_STATUS_INACTIVE int = iota
	USER_STATUS_ACTIVE
	USER_STATUS_BLOCKED
	USER_STATUS_DELETED
)

var userStatusNames = enum.Map {
	USER_STATUS_INACTIVE: "Неактивный",
	USER_STATUS_ACTIVE  : "Активный",
	USER_STATUS_BLOCKED : "Заблокированный",
	USER_STATUS_DELETED : "Удаленный",
}

var userStatusEnums = enum.List {
	USER_STATUS_INACTIVE: UserInactiveStatus(),
	USER_STATUS_ACTIVE  : UserActiveStatus(),
	USER_STATUS_BLOCKED : UserBlockedStatus(),
	USER_STATUS_DELETED : UserDeletedStatus(),
}

type UserStatus struct {
	*enum.BaseEnum
}

func (this *UserStatus) GetNames() enum.Map {
	return userStatusNames
}

func (this *UserStatus) GetList() enum.List {
	return userStatusEnums
}

func NewUserStatus(id int) *UserStatus {
	status := new(UserStatus)
	status.BaseEnum, _ = enum.Create(status, id)
	return status
}

func UserInactiveStatus() *UserStatus {
	return NewUserStatus(USER_STATUS_INACTIVE)
}

func UserActiveStatus() *UserStatus {
	return NewUserStatus(USER_STATUS_ACTIVE)
}

func UserBlockedStatus() *UserStatus {
	return NewUserStatus(USER_STATUS_BLOCKED)
}

func UserDeletedStatus() *UserStatus {
	return NewUserStatus(USER_STATUS_DELETED)
}

const (
	USER_ROLE_GUEST int = iota
	USER_ROLE_USER
	USER_ROLE_ADMIN
)

const (
	LOGGED_USER_MASK     = USER_ROLE_USER | USER_ROLE_ADMIN
	LOGGED_ADMIN_MASK    = USER_ROLE_ADMIN
)

var userRolesNames = enum.Map {
	USER_ROLE_GUEST: "Гость",
	USER_ROLE_USER : "Пользователь",
	USER_ROLE_ADMIN: "Администратор",
}

var userRolesEnums = enum.List {
	USER_ROLE_GUEST: UserRoleGuest(),
	USER_ROLE_USER : UserRoleUser(),
	USER_ROLE_ADMIN: UserRoleAdmin(),
}

type UserRole struct {
	*enum.BaseEnum
}

func (this *UserRole) GetNames() enum.Map {
	return userRolesNames
}

func (this *UserRole) GetList() enum.List {
	return userRolesEnums
}

func NewUserRole(id int) *UserRole {
	status := new(UserRole)
	status.BaseEnum, _ = enum.Create(status, id)
	return status
}

func UserRoleGuest() *UserRole {
	return NewUserRole(USER_ROLE_GUEST)
}

func UserRoleUser() *UserRole {
	return NewUserRole(USER_ROLE_USER)
}

func UserRoleAdmin() *UserRole {
	return NewUserRole(USER_ROLE_ADMIN)
}


