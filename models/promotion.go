package models

import "time"

type Promotions struct {
	Items []*Promotion `xml:"promotion"`
}

type Promotion struct {
	Id           int    `xml:"id"`
	Title        string `xml:"title"`
	Logo         string `xml:"logo"`
	OfferId      int    `xml:"offer_id"`
	OfferName    string `xml:"offer_name"`
	OfferLink    string `xml:"offer_link"`
	Categories   string `xml:"categories"`
	Description  string `xml:"description"`
	Code         string `xml:"code"`
	BeginDate    string `xml:"begin_date"`
	EndDate      string `xml:"end_date"`
	Status       string `xml:"status"`
	Exclusive    string `xml:"exclusive"`
	Rating       int    `xml:"rating"`
	TypeId       int    `xml:"type_id"`
	TypeName     string `xml:"type_name"`
	InFavourites string `xml:"in_favourites"`
	Link         string `xml:"link"`
}

func (this *Promotion) getBeginDate() time.Duration {
	duration, _ := time.ParseDuration(this.BeginDate)
	return duration
}

func (this *Promotion) getDuration(date string) *time.Duration {
	duration, err := time.ParseDuration(this.BeginDate)
	if err == nil {
		return &duration
	} else {
		return nil
	}
}

func (this *Promotion) getEndDate() time.Duration {
	duration, _ := time.ParseDuration(this.EndDate)
	return duration
}

