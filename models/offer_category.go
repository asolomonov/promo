package models

import (
	"time"
	"promo/models/enum"
)

type OfferCategory struct {
	Id           int       `orm:"pk;auto"`
	Name         string
	CreateDate   time.Time `orm:"auto_now_add;type(datetime)"`
	StatusId     int
	Path         string    `json:"-"`
	Offers       []*Offer  `orm:"reverse(many)"`
}

func NewOfferCategory() *OfferCategory {
	return new(OfferCategory)
}

func (this *OfferCategory) ById(id int) *OfferCategory {
	this.Id = id
	GetOrm().Read(this)
	return this
}

func (this *OfferCategory) ByName(name string) *OfferCategory {
	this.Name = name
	GetOrm().Read(this, "Name")
	return this
}

func (this *OfferCategory) ByPath(path string) *OfferCategory {
	this.Path = path
	GetOrm().Read(this, "Path")
	return this
}

func (this *OfferCategory) All() []*OfferCategory {
	var categories []*OfferCategory
	query := GetQuery(this)
	query.OrderBy("Name").All(&categories)
	return categories
}

func (this *OfferCategory) AllActive() []*OfferCategory {
	var categories []*OfferCategory
	query := GetQuery(this)
	query.Filter("StatusId", OFFER_CATEGORY_ACTIVE).OrderBy("Name").All(&categories)
	return categories
}

const (
	OFFER_CATEGORY_ADDED    int = iota
	OFFER_CATEGORY_ACTIVE
	OFFER_CATEGORY_INACTIVE
)

var categoryStatusNames = enum.Map {
	OFFER_CATEGORY_ADDED   : "Добавленный",
	OFFER_CATEGORY_ACTIVE  : "Активный",
	OFFER_CATEGORY_INACTIVE: "Неактивный",
}

var categoryStatusEnums = enum.List {
	OFFER_CATEGORY_ADDED   : OfferCategoryStatusAdded(),
	OFFER_CATEGORY_ACTIVE  : OfferCategoryStatusActive(),
	OFFER_CATEGORY_INACTIVE: OfferCategoryStatusInactive(),
}

type OfferCategoryStatus struct {
	*enum.BaseEnum
}

func (this *OfferCategoryStatus) GetNames() enum.Map {
	return categoryStatusNames
}

func (this *OfferCategoryStatus) GetList() enum.List {
	return categoryStatusEnums
}

func NewOfferCategoryStatus(id int) *OfferCategoryStatus {
	status := new(OfferCategoryStatus)
	status.BaseEnum, _ = enum.Create(status, id)
	return status
}

func OfferCategoryStatusAdded() *OfferCategoryStatus {
	return NewOfferCategoryStatus(OFFER_CATEGORY_ADDED)
}

func OfferCategoryStatusActive() *OfferCategoryStatus {
	return NewOfferCategoryStatus(OFFER_CATEGORY_ACTIVE)
}

func OfferCategoryStatusInactive() *OfferCategoryStatus {
	return NewOfferCategoryStatus(OFFER_CATEGORY_INACTIVE)
}

