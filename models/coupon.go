package models

import (
	"time"
	"promo/models/enum"
	"promo/util"
	"strings"
)

const (
	DAYS_IN_YEAR  = 356
	DAYS_IN_MONTH = 30
	HOURS_IN_DAY  = 24
)

type Coupon struct {
	Id               int       `orm:"pk;auto" json:"id"`
	ExternalId       int       `json:"-"`
	Offer            *Offer    `orm:"rel(fk)" json:"offer"`
	Code             string    `json:"-"`
	Name             string    `json:"name"`
	Description      string    `json:"description"`
	Logo             string    `json:"logo"`
	CreateDate       time.Time `orm:"auto_now_add;type(datetime)" json:"-"`
	BeginDate        time.Time `json:"-"`
	EndDate          time.Time `json:"-"`
	ExternalStatusId int       `json:"-"`
	StatusId         int       `json:"statusId"`
	Link             string    `json:"-"`
	Exclusive        bool      `json:"-"`
	Rating           int       `json:"-"`
	TypeId           int       `json:"-"`
	Rank             int       `json:"-"`
}

func NewCoupon() *Coupon {
	return new(Coupon)
}

func (this *Coupon) ById(id int) *Coupon {
	this.Id = id
	GetOrm().Read(this)
	return this
}

func (this *Coupon) ByExternalId(id int) *Coupon {
	this.ExternalId = id
	GetOrm().Read(this, "ExternalId")
	return this
}

func (this *Coupon) GetId() int {
	return this.Id
}

func (this *Coupon) GetTime() string {
	parts := make([]string, 0)
	diff := this.EndDate.Sub(time.Now())
	allDays := int(diff.Hours() / HOURS_IN_DAY)
	years := int(allDays / DAYS_IN_YEAR)
	months := int((allDays - years * DAYS_IN_YEAR) / DAYS_IN_MONTH)
	days := allDays - (years * DAYS_IN_YEAR + months * DAYS_IN_MONTH)
	hours := int(diff.Hours()) - allDays * HOURS_IN_DAY;
	if hours <= 0 {
		hours = 1;
	}
	if years > 0 {
		parts = append(parts, util.EndingByNum(years, []string{"год", "года", "лет"}))
	}
	if months > 0 {
		parts = append(parts, util.EndingByNum(months, []string{"месяц", "месяца", "месяцев"}))
	}
	if days > 0 {
		parts = append(parts, util.EndingByNum(days, []string{"день", "дня", "дней"}))
	}
	if days == 0 {
		parts = append(parts, util.EndingByNum(hours, []string{"час", "часа", "часов"}))
	}
	return strings.Join(parts, util.WHITE_SPACE)
}

func (this *Coupon) GetExternalStatus() *CouponExternalStatus {
	return NewCouponExternalStatus(this.ExternalStatusId)
}

const (
	COUPON_ADDED    int = iota
	COUPON_ACTIVE
	COUPON_INACTIVE
)

var statusNames = enum.Map {
	COUPON_ADDED   : "Добавленный",
	COUPON_ACTIVE  : "Активный",
	COUPON_INACTIVE: "Неактивный",
}

var statusEnums = enum.List {
	COUPON_ADDED   : CouponStatusAdded(),
	COUPON_ACTIVE  : CouponStatusActive(),
	COUPON_INACTIVE: CouponStatusInactive(),
}

type CouponStatus struct {
	*enum.BaseEnum
}

func (this *CouponStatus) GetNames() enum.Map {
	return statusNames
}

func (this *CouponStatus) GetList() enum.List {
	return statusEnums
}

func NewCouponStatus(id int) *CouponStatus {
	status := new(CouponStatus)
	status.BaseEnum, _ = enum.Create(status, id)
	return status
}

func CouponStatusAdded() *CouponStatus {
	return NewCouponStatus(COUPON_ADDED)
}

func CouponStatusActive() *CouponStatus {
	return NewCouponStatus(COUPON_ACTIVE)
}

func CouponStatusInactive() *CouponStatus {
	return NewCouponStatus(COUPON_INACTIVE)
}

const (
	COUPON_EXTERNAL_DISABLED              int = iota
	COUPON_EXTERNAL_ANNOUNCE
	COUPON_EXTERNAL_ACTIVE
	COUPON_EXTERNAL_STOPPED
	COUPON_EXTERNAL_EXPIRING
	COUPON_EXTERNAL_CREATED
	COUPON_EXTERNAL_WITH_INACTIVE_LANDING
)

var externalStatusNames = enum.Map {
	COUPON_EXTERNAL_DISABLED             : "Удаленый",
	COUPON_EXTERNAL_ANNOUNCE             : "Ожидается",
	COUPON_EXTERNAL_ACTIVE               : "Активный",
	COUPON_EXTERNAL_STOPPED              : "Завершеной",
	COUPON_EXTERNAL_EXPIRING             : "Истекает",
	COUPON_EXTERNAL_CREATED              : "Новый",
	COUPON_EXTERNAL_WITH_INACTIVE_LANDING: "С неактивным лендингом",
}

var externalStatusEnums = enum.List {
	COUPON_EXTERNAL_DISABLED             : NewCouponExternalStatus(COUPON_EXTERNAL_DISABLED),
	COUPON_EXTERNAL_ANNOUNCE             : NewCouponExternalStatus(COUPON_EXTERNAL_ANNOUNCE),
	COUPON_EXTERNAL_ACTIVE               : NewCouponExternalStatus(COUPON_EXTERNAL_ACTIVE),
	COUPON_EXTERNAL_STOPPED              : NewCouponExternalStatus(COUPON_EXTERNAL_STOPPED),
	COUPON_EXTERNAL_EXPIRING             : NewCouponExternalStatus(COUPON_EXTERNAL_EXPIRING),
	COUPON_EXTERNAL_CREATED              : NewCouponExternalStatus(COUPON_EXTERNAL_CREATED),
	COUPON_EXTERNAL_WITH_INACTIVE_LANDING: NewCouponExternalStatus(COUPON_EXTERNAL_WITH_INACTIVE_LANDING),
}

var externalStatusEnumsForHeader = enum.List {
	COUPON_EXTERNAL_ANNOUNCE             : NewCouponExternalStatus(COUPON_EXTERNAL_ANNOUNCE),
	COUPON_EXTERNAL_ACTIVE               : NewCouponExternalStatus(COUPON_EXTERNAL_ACTIVE),
	COUPON_EXTERNAL_EXPIRING             : NewCouponExternalStatus(COUPON_EXTERNAL_EXPIRING),
}

var externalStatusApNames = enum.Map {
	COUPON_EXTERNAL_DISABLED             : "Удалена",
	COUPON_EXTERNAL_ANNOUNCE             : "Ожидается",
	COUPON_EXTERNAL_ACTIVE               : "Активна",
	COUPON_EXTERNAL_STOPPED              : "Завершена",
	COUPON_EXTERNAL_EXPIRING             : "Истекает",
	COUPON_EXTERNAL_CREATED              : "Новая",
	COUPON_EXTERNAL_WITH_INACTIVE_LANDING: "С неактивным лендингом",
}

type CouponExternalStatus struct {
	*enum.BaseEnum
}

func (this *CouponExternalStatus) GetNames() enum.Map {
	return externalStatusNames
}

func (this *CouponExternalStatus) GetList() enum.List {
	return externalStatusEnums
}

func (this *CouponExternalStatus) GetHeaderList() enum.List {
	return externalStatusEnumsForHeader
}

func (this *CouponExternalStatus) GetExternalNames() enum.Map {
	return externalStatusApNames
}

func NewCouponExternalStatus(id int) *CouponExternalStatus {
	status := new(CouponExternalStatus)
	status.BaseEnum, _ = enum.Create(status, id)
	return status
}

func CouponExternalStatusActive() *CouponExternalStatus {
	return NewCouponExternalStatus(COUPON_EXTERNAL_ACTIVE)
}

const (
	COUPON_TYPE_FREE_SHIPPING     int = iota
	COUPON_TYPE_MONEY_AS_GIFT
	COUPON_TYPE_GIFT_TO_ORDER
	COUPON_TYPE_DISCOUNT_ON_ORDER
	COUPON_TYPE_OTHER             = 98
)

var couponTypeNames = enum.Map {
	COUPON_TYPE_FREE_SHIPPING    : "Бесплатная доставка",
	COUPON_TYPE_MONEY_AS_GIFT    : "Деньги в подарок",
	COUPON_TYPE_GIFT_TO_ORDER    : "Подарок к заказу",
	COUPON_TYPE_DISCOUNT_ON_ORDER: "Скидка на заказ",
	COUPON_TYPE_OTHER            : "Другое",
}

var couponTypeEnums = enum.List {
	COUPON_TYPE_FREE_SHIPPING    : NewCouponType(COUPON_TYPE_FREE_SHIPPING),
	COUPON_TYPE_MONEY_AS_GIFT    : NewCouponType(COUPON_TYPE_MONEY_AS_GIFT),
	COUPON_TYPE_GIFT_TO_ORDER    : NewCouponType(COUPON_TYPE_GIFT_TO_ORDER),
	COUPON_TYPE_DISCOUNT_ON_ORDER: NewCouponType(COUPON_TYPE_DISCOUNT_ON_ORDER),
	COUPON_TYPE_OTHER            : NewCouponType(COUPON_TYPE_OTHER),
}

type CouponType struct {
	*enum.BaseEnum
}

func (this *CouponType) GetNames() enum.Map {
	return couponTypeNames
}

func (this *CouponType) GetList() enum.List {
	return couponTypeEnums
}

func NewCouponType(id int) *CouponType {
	status := new(CouponType)
	status.BaseEnum, _ = enum.Create(status, id)
	return status
}

func CouponTypeOther() *CouponType {
	return NewCouponType(COUPON_TYPE_OTHER)
}

const (
	COUPON_SORT_CREATE_DATE_ASC int = iota + 1
	COUPON_SORT_RATING_ASC
	COUPON_SORT_RATING_DESC
	COUPON_SORT_END_DATE_ASC
	COUPON_SORT_END_DATE_DESC
)

var couponSortNames = enum.Map {
	COUPON_SORT_CREATE_DATE_ASC: "Новые",
	COUPON_SORT_RATING_ASC     : "Рейтинг - по возростанию",
	COUPON_SORT_RATING_DESC    : "Рейтинг - по убыванию",
	COUPON_SORT_END_DATE_ASC   : "Дата окончания - по возростанию",
	COUPON_SORT_END_DATE_DESC  : "Дата окончания - по убыванию",
}

var couponSortEnums = enum.List {
	COUPON_SORT_CREATE_DATE_ASC: NewCouponSort(COUPON_SORT_CREATE_DATE_ASC),
	COUPON_SORT_RATING_ASC     : NewCouponSort(COUPON_SORT_RATING_ASC),
	COUPON_SORT_RATING_DESC    : CouponSortRatingDesc(),
	COUPON_SORT_END_DATE_ASC   : NewCouponSort(COUPON_SORT_END_DATE_ASC),
	COUPON_SORT_END_DATE_DESC  : NewCouponSort(COUPON_SORT_END_DATE_DESC),
}

var couponSortFields = map[int][]string {
	COUPON_SORT_CREATE_DATE_ASC: []string{"-CreateDate", "Rank"},
	COUPON_SORT_RATING_ASC     : []string{"Rating", "Rank"},
	COUPON_SORT_RATING_DESC    : []string{"-Rating", "Rank"},
	COUPON_SORT_END_DATE_ASC   : []string{"EndDate", "Rank"},
	COUPON_SORT_END_DATE_DESC  : []string{"-EndDate", "Rank"},
}

type CouponSort struct {
	*enum.BaseEnum
}

func (this *CouponSort) GetNames() enum.Map {
	return couponSortNames
}

func (this *CouponSort) GetList() enum.List {
	return couponSortEnums
}

func (this *CouponSort) GetFields() []string {
	return couponSortFields[this.GetId()]
}

func NewCouponSort(id int) *CouponSort {
	status := new(CouponSort)
	status.BaseEnum, _ = enum.Create(status, id)
	return status
}

func CouponSortRatingDesc() *CouponSort {
	return NewCouponSort(COUPON_SORT_RATING_DESC)
}
