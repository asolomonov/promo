package enum

import "errors"

type Map map[int]string
type List map[int]Enum

type Enum interface {
	GetId() int
	GetName() string
	SetId(int) Enum
	SetName(string) Enum
}

type EnumList interface {
	Enum
	GetList()  List
	GetNames() Map
}

type BaseEnum struct {
	Id   int
	Name string
}

func (this *BaseEnum) GetId() int {
	return this.Id
}

func (this *BaseEnum) GetName() string {
	return this.Name
}

func (this *BaseEnum) SetId(id int) Enum {
	this.Id = id
	return this
}

func (this *BaseEnum) SetName(name string) Enum {
	this.Name = name
	return this
}

func Create(enum EnumList, id int) (*BaseEnum, error) {
	names := enum.GetNames()
	if name, ok := names[id]; ok {
		return &BaseEnum{id, name}, nil
	} else {
		return nil, errors.New("unknow enum id")
	}
}

