package models

import (
	_ "github.com/lib/pq"
	"github.com/byorty/beego/orm"
	"github.com/byorty/beego"
	"reflect"
	"promo/util"
)

const (
	INVALID_ID = 0
)

var ormer orm.Ormer

func Init() {
	orm.RegisterModel(
		new(Offer),
		new(OfferCategory),
		new(Coupon),
		new(User),
	)
	orm.RegisterDriver("postgres", orm.DR_Postgres)
	maxIdle, _ := beego.AppConfig.Int("postgres.default.maxIdleConns")
	maxOpen, _ := beego.AppConfig.Int("postgres.default.maxOpenConns")
	orm.RegisterDataBase("default", "postgres", beego.AppConfig.String("postgres.default"), maxIdle, maxOpen)
	if !util.IS_PRODUCTION {
		orm.Debug = true
	}
	ormer = orm.NewOrm()
}

func GetOrm() orm.Ormer {
	return ormer
}

func GetQuery(model interface{}) orm.QuerySeter {
	value := getValue(model)
	return GetOrm().QueryTable(getTypeName(value))
}

func getTypeName(value reflect.Value) string {
	return value.Type().Name()
}

func getValue(model interface{}) reflect.Value {
	value := reflect.ValueOf(model)
	if value.Kind() == reflect.Ptr {
		value = reflect.Indirect(value)
	}
	return value
}
