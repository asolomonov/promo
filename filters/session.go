package filters

import (
	"github.com/byorty/beego/context"
	"promo/controllers"
	"promo/models"
	"promo/util"
)

func initSessionFilter(ctx *context.Context) {
	var sessionUser *models.User
	val := ctx.Input.Session("user")
	if val == nil {
		sessionUser = models.NewUser()
		sessionUser.StatusId = models.USER_STATUS_ACTIVE
		sessionUser.RoleId = models.USER_ROLE_GUEST
		ctx.Output.Session("user", sessionUser)
	}
}

func guestAccessFilter(ctx *context.Context) {
	if isAccessDenied(ctx, models.LOGGED_USER_MASK) {
		ctx.Redirect(controllers.REDIRECT_CODE, util.URL_ROOT)
	}
}

func userAccessFilter(ctx *context.Context) {
	if isAccessDenied(ctx, models.USER_ROLE_GUEST) {
		ctx.Redirect(controllers.REDIRECT_CODE, util.URL_SINGIN)
	}
}

func adminAccessFilter(ctx *context.Context) {
	if isAccessDenied(ctx, models.USER_ROLE_ADMIN) {
		ctx.Redirect(controllers.REDIRECT_CODE, util.URL_SINGIN)
	}
}

func isAccessAllowed(ctx *context.Context, roles int) bool {
	return !isAccessDenied(ctx, roles)
}

func getUser(ctx *context.Context) *models.User {
	user := ctx.Input.Session("user")
	if user == nil {
		return nil
	} else {
		return user.(*models.User)
	}
}

func isAccessDenied(ctx *context.Context, roles int) bool {
	return (roles & getUser(ctx).RoleId) == 0
}




