package filters

import "github.com/byorty/beego"

func init() {
	beego.InsertFilter("/", beego.BeforeExec, initSessionFilter)
	beego.InsertFilter("/*", beego.BeforeExec, initSessionFilter)
	beego.InsertFilter("/admin/*", beego.BeforeExec, adminAccessFilter)
	beego.InsertFilter("/api/admin/*", beego.BeforeExec, adminAccessFilter)
}


