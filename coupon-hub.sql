--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.4
-- Dumped by pg_dump version 9.3.1
-- Started on 2014-08-02 10:19:24 MSK

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 180 (class 3079 OID 12018)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2265 (class 0 OID 0)
-- Dependencies: 180
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 170 (class 1259 OID 25419)
-- Name: coupon_id; Type: SEQUENCE; Schema: public; Owner: byorty
--

CREATE SEQUENCE coupon_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.coupon_id OWNER TO byorty;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 171 (class 1259 OID 25421)
-- Name: coupon; Type: TABLE; Schema: public; Owner: byorty; Tablespace: 
--

CREATE TABLE coupon (
    id bigint DEFAULT nextval('coupon_id'::regclass) NOT NULL,
    offer_id bigint NOT NULL,
    code character varying(64),
    name text NOT NULL,
    description text NOT NULL,
    logo text,
    create_date timestamp without time zone NOT NULL,
    begin_date timestamp without time zone NOT NULL,
    end_date timestamp without time zone NOT NULL,
    status_id bigint NOT NULL,
    exclusive boolean DEFAULT false NOT NULL,
    rating smallint DEFAULT (1)::smallint NOT NULL,
    type_id bigint NOT NULL,
    external_status_id bigint NOT NULL,
    external_id integer NOT NULL,
    rank integer NOT NULL,
    link text
);


ALTER TABLE public.coupon OWNER TO byorty;

--
-- TOC entry 175 (class 1259 OID 25445)
-- Name: offer_id; Type: SEQUENCE; Schema: public; Owner: byorty
--

CREATE SEQUENCE offer_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.offer_id OWNER TO byorty;

--
-- TOC entry 172 (class 1259 OID 25430)
-- Name: offer; Type: TABLE; Schema: public; Owner: byorty; Tablespace: 
--

CREATE TABLE offer (
    id bigint DEFAULT nextval('offer_id'::regclass) NOT NULL,
    external_id integer NOT NULL,
    name character varying(255) NOT NULL,
    short_name character varying(255) NOT NULL,
    logo character varying(255) NOT NULL,
    link character varying(255) NOT NULL,
    create_date timestamp with time zone NOT NULL,
    status_id integer NOT NULL,
    path character varying(255) NOT NULL
);


ALTER TABLE public.offer OWNER TO byorty;

--
-- TOC entry 173 (class 1259 OID 25436)
-- Name: offer_category_id; Type: SEQUENCE; Schema: public; Owner: byorty
--

CREATE SEQUENCE offer_category_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.offer_category_id OWNER TO byorty;

--
-- TOC entry 174 (class 1259 OID 25438)
-- Name: offer_category; Type: TABLE; Schema: public; Owner: byorty; Tablespace: 
--

CREATE TABLE offer_category (
    id bigint DEFAULT nextval('offer_category_id'::regclass) NOT NULL,
    name text NOT NULL,
    create_date timestamp without time zone NOT NULL,
    status_id bigint NOT NULL,
    path character varying(255) NOT NULL
);


ALTER TABLE public.offer_category OWNER TO byorty;

--
-- TOC entry 176 (class 1259 OID 25449)
-- Name: offer_offer_categorys_id; Type: SEQUENCE; Schema: public; Owner: byorty
--

CREATE SEQUENCE offer_offer_categorys_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.offer_offer_categorys_id OWNER TO byorty;

--
-- TOC entry 177 (class 1259 OID 25451)
-- Name: offer_offer_categorys; Type: TABLE; Schema: public; Owner: byorty; Tablespace: 
--

CREATE TABLE offer_offer_categorys (
    offer_id integer NOT NULL,
    offer_category_id integer NOT NULL,
    id bigint DEFAULT nextval('offer_offer_categorys_id'::regclass) NOT NULL
);


ALTER TABLE public.offer_offer_categorys OWNER TO byorty;

--
-- TOC entry 178 (class 1259 OID 25455)
-- Name: user_id; Type: SEQUENCE; Schema: public; Owner: byorty
--

CREATE SEQUENCE user_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id OWNER TO byorty;

--
-- TOC entry 179 (class 1259 OID 25457)
-- Name: user; Type: TABLE; Schema: public; Owner: byorty; Tablespace: 
--

CREATE TABLE "user" (
    id bigint DEFAULT nextval('user_id'::regclass) NOT NULL,
    email character varying(100) NOT NULL,
    password character varying(32) NOT NULL,
    role_id integer NOT NULL,
    status_id integer NOT NULL,
    register_date timestamp with time zone NOT NULL,
    login_ip text,
    login_date character varying(255) NOT NULL,
    user_agent text
);


ALTER TABLE public."user" OWNER TO byorty;

--
-- TOC entry 2249 (class 0 OID 25421)
-- Dependencies: 171
-- Data for Name: coupon; Type: TABLE DATA; Schema: public; Owner: byorty
--

COPY coupon (id, offer_id, code, name, description, logo, create_date, begin_date, end_date, status_id, exclusive, rating, type_id, external_status_id, external_id, rank, link) FROM stdin;
\.


--
-- TOC entry 2266 (class 0 OID 0)
-- Dependencies: 170
-- Name: coupon_id; Type: SEQUENCE SET; Schema: public; Owner: byorty
--

SELECT pg_catalog.setval('coupon_id', 1, true);


--
-- TOC entry 2250 (class 0 OID 25430)
-- Dependencies: 172
-- Data for Name: offer; Type: TABLE DATA; Schema: public; Owner: byorty
--

COPY offer (id, external_id, name, short_name, logo, link, create_date, status_id, path) FROM stdin;
\.


--
-- TOC entry 2252 (class 0 OID 25438)
-- Dependencies: 174
-- Data for Name: offer_category; Type: TABLE DATA; Schema: public; Owner: byorty
--

COPY offer_category (id, name, create_date, status_id, path) FROM stdin;
\.


--
-- TOC entry 2267 (class 0 OID 0)
-- Dependencies: 173
-- Name: offer_category_id; Type: SEQUENCE SET; Schema: public; Owner: byorty
--

SELECT pg_catalog.setval('offer_category_id', 1, true);


--
-- TOC entry 2268 (class 0 OID 0)
-- Dependencies: 175
-- Name: offer_id; Type: SEQUENCE SET; Schema: public; Owner: byorty
--

SELECT pg_catalog.setval('offer_id', 1, true);


--
-- TOC entry 2255 (class 0 OID 25451)
-- Dependencies: 177
-- Data for Name: offer_offer_categorys; Type: TABLE DATA; Schema: public; Owner: byorty
--

COPY offer_offer_categorys (offer_id, offer_category_id, id) FROM stdin;
\.


--
-- TOC entry 2269 (class 0 OID 0)
-- Dependencies: 176
-- Name: offer_offer_categorys_id; Type: SEQUENCE SET; Schema: public; Owner: byorty
--

SELECT pg_catalog.setval('offer_offer_categorys_id', 1, true);


--
-- TOC entry 2257 (class 0 OID 25457)
-- Dependencies: 179
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: byorty
--

COPY "user" (id, email, password, role_id, status_id, register_date, login_ip, login_date, user_agent) FROM stdin;
1	byorty@mail.ru	1a033f83391a4a8b7c2017cd73029b8c	2	1	2014-07-09 02:33:51.6405+04	127.0.0.1		Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36 FirePHP/4Chrome
\.


--
-- TOC entry 2270 (class 0 OID 0)
-- Dependencies: 178
-- Name: user_id; Type: SEQUENCE SET; Schema: public; Owner: byorty
--

SELECT pg_catalog.setval('user_id', 1, true);


--
-- TOC entry 2132 (class 2606 OID 25466)
-- Name: offer_category_path_key; Type: CONSTRAINT; Schema: public; Owner: byorty; Tablespace: 
--

ALTER TABLE ONLY offer_category
    ADD CONSTRAINT offer_category_path_key UNIQUE (path);


--
-- TOC entry 2134 (class 2606 OID 25468)
-- Name: offer_category_pkey; Type: CONSTRAINT; Schema: public; Owner: byorty; Tablespace: 
--

ALTER TABLE ONLY offer_category
    ADD CONSTRAINT offer_category_pkey PRIMARY KEY (id);


--
-- TOC entry 2136 (class 2606 OID 25470)
-- Name: offer_offer_categorys_pkey; Type: CONSTRAINT; Schema: public; Owner: byorty; Tablespace: 
--

ALTER TABLE ONLY offer_offer_categorys
    ADD CONSTRAINT offer_offer_categorys_pkey PRIMARY KEY (id);


--
-- TOC entry 2128 (class 2606 OID 25472)
-- Name: offer_path_key; Type: CONSTRAINT; Schema: public; Owner: byorty; Tablespace: 
--

ALTER TABLE ONLY offer
    ADD CONSTRAINT offer_path_key UNIQUE (path);


--
-- TOC entry 2130 (class 2606 OID 25500)
-- Name: offer_pkey; Type: CONSTRAINT; Schema: public; Owner: byorty; Tablespace: 
--

ALTER TABLE ONLY offer
    ADD CONSTRAINT offer_pkey PRIMARY KEY (id);


--
-- TOC entry 2126 (class 2606 OID 25476)
-- Name: promocode_pkey; Type: CONSTRAINT; Schema: public; Owner: byorty; Tablespace: 
--

ALTER TABLE ONLY coupon
    ADD CONSTRAINT promocode_pkey PRIMARY KEY (id);


--
-- TOC entry 2138 (class 2606 OID 25478)
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: byorty; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- TOC entry 2124 (class 1259 OID 25536)
-- Name: coupon_offer_id_idx; Type: INDEX; Schema: public; Owner: byorty; Tablespace: 
--

CREATE INDEX coupon_offer_id_idx ON coupon USING btree (offer_id);


--
-- TOC entry 2139 (class 2606 OID 25501)
-- Name: coupon_offer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: byorty
--

ALTER TABLE ONLY coupon
    ADD CONSTRAINT coupon_offer_id_fkey FOREIGN KEY (offer_id) REFERENCES offer(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2140 (class 2606 OID 25484)
-- Name: offer_offer_categorys_offer_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: byorty
--

ALTER TABLE ONLY offer_offer_categorys
    ADD CONSTRAINT offer_offer_categorys_offer_category_id_fkey FOREIGN KEY (offer_category_id) REFERENCES offer_category(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2264 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: byorty
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM byorty;
GRANT ALL ON SCHEMA public TO byorty;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2014-08-02 10:19:25 MSK

--
-- PostgreSQL database dump complete
--

