package util

import (
	"regexp"
	"strings"
	"fmt"
)

const (
	WHITE_SPACE   = " "
	COMMA         = ", "
	DASH          = "-"
	INVALID_INDEX = -1
)

var (
	specRegexp = regexp.MustCompile(`[^\w\d\sА-Яа-я]`)
	whiteSpaceRegexp = regexp.MustCompile(`\s+`)
	dashRegexp = regexp.MustCompile(`\-+`)
	transliteMap = map[string]string{
		"а": "a",  "А": "a",
		"б": "b",  "Б": "b",
		"в": "v",  "В": "v",
		"г": "g",  "Г": "g",
		"д": "d",  "Д": "d",
		"е": "e",  "Е": "e",
		"ё": "e",  "Ё": "e",
		"ж": "zh", "Ж": "zh",
		"з": "z",  "З": "z",
		"и": "i",  "И": "i",
		"й": "y",  "Й": "y",
		"к": "k",  "К": "k",
		"л": "l",  "Л": "l",
		"м": "m",  "М": "m",
		"н": "n",  "Н": "n",
		"о": "o",  "О": "o",
		"п": "p",  "П": "p",
		"р": "r",  "Р": "r",
		"с": "s",  "С": "s",
		"т": "t",  "Т": "t",
		"у": "u",  "У": "u",
		"ф": "f",  "Ф": "f",
		"х": "h",  "Х": "h",
		"ц": "c",  "Ц": "c",
		"ч": "ch", "Ч": "ch",
		"ш": "sh", "Ш": "sh",
		"щ": "sch","Щ": "sch",
		"ъ": "",   "Ъ": "",
		"ы": "y",  "Ы": "y",
		"ь": "",   "Ь": "",
		"э": "e",  "Э": "e",
		"ю": "yu", "Ю": "yu",
		"я": "ya", "Я": "ya",
	}
)

func ClearString(str string) string {
	str = specRegexp.ReplaceAllString(str, WHITE_SPACE)
	str = whiteSpaceRegexp.ReplaceAllString(str, WHITE_SPACE)
	return str
}

func TransliteString(str string) string {
	for ruChar, enChar := range transliteMap {
		str = strings.Replace(str, ruChar, enChar, -1)
	}
	return str
}

func ToSeoPath(str string) string {
	str = strings.TrimSpace(str)
	str = TransliteString(str)
	str = ClearString(str)
	str = whiteSpaceRegexp.ReplaceAllString(str, DASH)
	str = dashRegexp.ReplaceAllString(str, DASH)
	return strings.ToLower(str)
}

func EndingByNum(number int, variants []string) string {
	var ending string
	div := number % 100;
	if div >= 11 && div <= 19 {
		ending = variants[2];
	} else {
		i := div % 10;
		switch (i) {
		case 1:
			ending = variants[0];
		case 2, 3, 4:
			ending = variants[1];
		default:
			ending = variants[2];
		}
	}
	return fmt.Sprintf("%d %s", number, ending)
}

func SubLastWhiteSpace(str string, maxLength int) string {
	if len(str) > maxLength {
		str = str[0:maxLength]
		lastIndex := strings.LastIndex(str, WHITE_SPACE)
		if lastIndex == INVALID_INDEX {
			return Ellipsis(str)
		} else {
			return Ellipsis(str[0:lastIndex])
		}
	} else {
		return str
	}
}

func Ellipsis(str string) string {
	return fmt.Sprintf("%s ...", str)
}

