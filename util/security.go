package util

import (
	"math/rand"
	"bytes"
	"crypto/md5"
	"io"
	"fmt"
	"github.com/byorty/beego"
	"time"
)

func RandomInt(min, max int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(max - min) + min
}

func RandomString(charset string, length int) string {
	charsetLen := len(charset)
	buf := new(bytes.Buffer)
	for i := 0; i < length; i++ {
		buf.WriteByte(charset[RandomInt(0, charsetLen)])
	}
	return buf.String()
}

func CreatePassword() string {
	return RandomString(beego.AppConfig.String("password.charset"), 10)
}

func GetHashedPassword(str string) string {
	return Md5(fmt.Sprintf("%s:%s", str, beego.AppConfig.String("password.salt")))
}

func Md5(str string) string {
	h := md5.New()
	io.WriteString(h, str)
	return fmt.Sprintf("%x", h.Sum(nil))
}


