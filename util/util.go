package util

import (
	"github.com/byorty/beego"
)

var (
	IS_PRODUCTION = beego.AppConfig.String("runmode") == "prod"
)
