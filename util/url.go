package util

import (
	"github.com/byorty/beego/context"
	"fmt"
	"net/url"
	"bytes"
	"strings"
)

const (
	URL_ROOT = "/"
	URL_SINGIN = "/signin"
	URL_SINGOUT = "/signout"
	URL_ADMIN_DASHBOARD = "/admin/dashboard"
	URL_ADMIN_CATEGORIES = "/admin/categories"
	URL_ADMIN_OFFERS = "/admin/offers"
	URL_ADMIN_OFFER = "/admin/offer"
	URL_ADMIN_COUPONS = "/admin/coupons"
	URL_ADMIN_COUPON = "/admin/coupon"
)

type Href struct {
	context        *context.Context
	pathParams     map[string]interface{}
	params         map[string]interface{}
	dropParams     []string
	dropPathParams []string
	ignoreQuery    bool
	ignorePath     bool
	host           string
}

func NewHref(context *context.Context) *Href {
	h := new(Href)
	h.context = context
	h.params = make(map[string]interface{})
	h.pathParams = make(map[string]interface{})
	h.dropParams = make([]string, 0)
	h.dropPathParams = make([]string, 0)
	h.ignoreQuery = false
	h.ignorePath = false
	return h
}

func (this Href) Set(key string, value interface{}) Href {
	this.params[key] = value
	return this
}

func (this Href) Drop(key string) Href {
	this.dropParams = append(this.dropParams, key)
	return this
}

func (this Href) Path(path string) Href {
	this.ignoreQuery = true
	this.ignorePath = true
	return this.SetPathParam(path, nil)
}

func (this Href) SetPathParam(key string, value interface{}) Href {
	this.pathParams[key] = value
	return this
}

func (this Href) HasPathParam(key string) bool {
	_, ok := this.pathParams[key]
	return ok
}

func (this Href) DropPathParam(key string) Href {
	this.dropPathParams = append(this.dropPathParams, key)
	return this
}

func (this Href) SetHost(host string) Href {
	this.host = host
	return this
}

func (this Href) Root() Href {
	return this.Path(URL_ROOT)
}

func (this Href) Signin() Href {
	return this.Path(URL_SINGIN)
}

func (this Href) Signout() Href {
	return this.Path(URL_SINGOUT)
}

func (this Href) AdminDashboard() Href {
	return this.Path(URL_ADMIN_DASHBOARD)
}

func (this Href) String() string {
	requestUrl := this.context.Request.URL
	input := this.context.Input
	currentUrl := new(url.URL)
	currentUrl.Scheme = input.Scheme()
	if len(this.host) == 0 {
		port := this.context.Input.Port()
		if port == 80 {
			currentUrl.Host = input.Host()
		} else {
			currentUrl.Host = fmt.Sprintf("%s:%d", input.Host(), port)
		}
	} else {
		currentUrl.Host = this.host
	}

	resultPaths := make(map[string]interface{})
	if !this.ignorePath {
		requestPartParams := make(map[string]interface{})
		requestPaths := strings.Split(requestUrl.Path, "/")
		requestPathsLen := len(requestPaths) - 1
		for i := 1;i < requestPathsLen;i = i + 2 {
			if i + 1 <= requestPathsLen  {
				requestPartParams[requestPaths[i]] = requestPaths[i + 1]
			} else {
				if requestPaths[i] != "/" {
					requestPartParams[requestPaths[i]] = nil
				}
			}
		}
		this.mergePaths(&requestPartParams, &resultPaths)
	}
	this.mergePaths(&this.pathParams, &resultPaths)

	for _, path := range this.dropPathParams {
		delete(resultPaths, path)
	}
	this.dropPathParams = this.dropPathParams[:cap(this.dropPathParams)]

	if len(resultPaths) > 0 {
		buf := new(bytes.Buffer)
		for key, value := range resultPaths {
			if value == nil {
				if key != "/" {
					buf.WriteString(fmt.Sprintf("%s/", key))
				}
			} else {
				buf.WriteString(fmt.Sprintf("%s/%v/", key, value))
			}
		}
		currentUrl.Path = buf.String()
	}

	if this.ignoreQuery == false {
		values, _ := url.ParseQuery(requestUrl.RawQuery)
		for key, value := range this.params {
			values.Set(key, fmt.Sprintf("%v", value))
			delete(this.params, key)
		}
		for _, key := range this.dropParams {
			values.Del(key)
		}
		this.dropParams = this.dropParams[:cap(this.dropParams)]
		currentUrl.RawQuery = values.Encode()
	} else {
		this.ignoreQuery = false
	}
	return currentUrl.String()
}

func (this Href) mergePaths(srcPathsPtr, resultPathsPtr *map[string]interface{}) {
	srcPaths := *srcPathsPtr
	resultPaths := *resultPathsPtr
	for key, value := range srcPaths {
		resultPaths[key] = value
		delete(srcPaths, key)
	}
}

type AngularRouter struct {
	Name       string
	Href       string
	Controller string
	Template   string
}

var adminHeaderRouters = make([]*AngularRouter, 0)
var adminRouters = make([]*AngularRouter, 0)

func GetAdminHeaderRouters(context *context.Context) []*AngularRouter {
	if len(adminHeaderRouters) == 0 {
		adminHeaderRouters = append(adminHeaderRouters, &AngularRouter{"Сводка", URL_ADMIN_DASHBOARD, "DashboardCtrl", "dashboard.html"})
		adminHeaderRouters = append(adminHeaderRouters, &AngularRouter{"Категории", URL_ADMIN_CATEGORIES, "CategoriesCtrl", "categories.html"})
		adminHeaderRouters = append(adminHeaderRouters, &AngularRouter{"Офферы", URL_ADMIN_OFFERS, "OffersCtrl", "offers.html"})
		adminHeaderRouters = append(adminHeaderRouters, &AngularRouter{"Купоны", URL_ADMIN_COUPONS, "CouponsCtrl", "coupons.html"})
	}
	return adminHeaderRouters
}

func GetAdminRouters(context *context.Context) []*AngularRouter {
	if len(adminRouters) == 0 {
		adminRouters = append(adminRouters, GetAdminHeaderRouters(context)...)
		adminRouters = append(adminRouters, &AngularRouter{"Оффер", URL_ADMIN_OFFER, "OfferCtrl", "offer.html"})
		adminRouters = append(adminRouters, &AngularRouter{"Купон", URL_ADMIN_COUPON, "CouponCtrl", "coupon.html"})
	}
	return adminRouters
}
