package util

import (
	"github.com/byorty/beego"
	"html/template"
)

func init() {
	beego.AddFuncMap("toJs", ToJs)
	beego.AddFuncMap("overflowEllipsis", SubLastWhiteSpace)
}

func ToJs(str string) template.JS {
	return template.JS(str)
}
