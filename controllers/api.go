package controllers

import (
	"promo/models"
	"fmt"
	"github.com/byorty/beego"
	"strings"
	"promo/util"
	"os"
	"image/jpeg"
	"github.com/nfnt/resize"
	"image/png"
	"image/gif"
	"image"
	"time"
	"net/url"
)

const (
	IMG_PATH = "./static/img/%s"
)

type response map[string]interface{}

type ApiController struct {
	MainController
}

func (this *ApiController) showSuccess(resp response) {
	resp["success"] = true
	paginator := this.GetPaginator()
	if paginator != nil {
		resp["paginator"] = paginator.ToMap()
	}
	this.Data["json"] = resp
	this.ServeJson()
}

type ApiAdminDashboardController struct {
	ApiController
}

func (this *ApiAdminDashboardController) Get() {
	categoriesCount, _ := models.GetQuery(models.NewOfferCategory()).Filter("StatusId", models.OFFER_CATEGORY_ADDED).Count()
	offersCount, _ := models.GetQuery(models.NewOffer()).Filter("StatusId", models.OFFER_ADDED).Count()
	couponsCount, _ := models.GetQuery(models.NewCoupon()).Filter("StatusId", models.COUPON_ADDED).Count()
	this.showSuccess(response{
		"categories": categoriesCount,
		"offers": offersCount,
		"coupons": couponsCount,
	})
}

type ApiAdminCouponsForm struct {
	Id int `form:"id"`
}

type ApiAdminCouponsController struct {
	ApiController
}

func (this *ApiAdminCouponsController) Get() {
	couponsCount, _ := models.GetQuery(models.NewCoupon()).Count()
	paginator := this.NewPaginator(couponsCount)
	var coupons []*models.Coupon
	models.GetQuery(models.NewCoupon()).
		Limit(paginator.PerPageNums).
		Offset(paginator.Offset()).
		RelatedSel().
		All(&coupons)
	this.showSuccess(response{
		"items": coupons,
	})
}

func (this *ApiAdminCouponsController) Post() {
	form := new(ApiAdminCouponsForm)
	this.ParseForm(form)
	_, header, err := this.GetFile("file")
	if err == nil {
		coupon := models.NewCoupon().ById(form.Id)
		if coupon.Id == models.INVALID_ID {
			beego.Critical(fmt.Sprintf("coupon #%d not found", form.Id))
		} else {
			if len(coupon.Logo) > 0 {
				os.Remove(fmt.Sprintf(IMG_PATH, coupon.Logo))
			}
			filenameParts := strings.Split(header.Filename, ".")
			filename := util.Md5(fmt.Sprintf("%s%d%d%d", filenameParts[0], form.Id, this.User.Id, time.Now().Unix()))
			tmpFilename := fmt.Sprintf("%s.%s", filename, "tmp")
			err := this.SaveToFile("file", fmt.Sprintf(IMG_PATH, tmpFilename))
			if err != nil {
				beego.Critical(err)
			}
			file, err := os.Open(fmt.Sprintf(IMG_PATH, tmpFilename))
			if err != nil {
				beego.Critical(err)
			}
			defer file.Close()
			var img image.Image
			switch (filenameParts[len(filenameParts) - 1]) {
			case "png":
				img, err = png.Decode(file)
			case "jpg", "jpeg":
				img, err = jpeg.Decode(file)
			case "gif":
				img, err = gif.Decode(file)
			default:
				beego.Critical("unknow image format")
			}
			if err != nil {
				beego.Critical(err)
			}
			// resize to width 1000 using Lanczos resampling
			// and preserve aspect ratio
			m := resize.Thumbnail(240, 500, img, resize.NearestNeighbor)

			filename = util.Md5(fmt.Sprintf("%s.%s", filename, "resized"))
			logoFilename := fmt.Sprintf("%s.%s", filename, "jpg")
			out, err := os.Create(fmt.Sprintf(IMG_PATH, logoFilename))
			if err != nil {
				beego.Critical(err)
			}
			defer out.Close()
			// write new image to file
			err = jpeg.Encode(out, m, nil)
			if err != nil {
				beego.Critical(err)
			}
			os.Remove(fmt.Sprintf(IMG_PATH, tmpFilename))
			coupon.Logo = logoFilename
			coupon.StatusId = models.COUPON_ACTIVE
			models.GetOrm().Update(coupon)
		}
	} else {
		beego.Critical(err)
	}
	this.ServeJson()
}

type ApiAdminOffersForm struct {
	Id        int    `form:"id"`
	Name      string `form:"name"`
	ShortName string `form:"shortName"`
	Link      string `form:"link"`
}

type ApiAdminOffersController struct {
	ApiController
}

func (this *ApiAdminOffersController) Get() {
	form := new(ApiAdminOffersForm)
	this.ParseForm(form)
	if form.Id == models.INVALID_ID {
		count, _ := models.GetQuery(models.NewOffer()).Count()
		paginator := this.NewPaginator(count)
		var offers []*models.Offer
		models.GetQuery(models.NewOffer()).
			Limit(paginator.PerPageNums).
			Offset(paginator.Offset()).
			RelatedSel().
			All(&offers)
		this.showSuccess(response{
			"items": offers,
		})
	} else {
		this.showSuccess(response{
			"item": models.NewOffer().ById(form.Id),
		})
	}
}

func (this *ApiAdminOffersController) Post() {
	form := new(ApiAdminOffersForm)
	values, _ := url.ParseQuery(string(this.Ctx.Input.RequestBody))
	beego.ParseForm(values, form)
	offer := models.NewOffer().ById(form.Id)
	offer.Name = form.Name
	offer.ShortName = form.ShortName
	offer.Link = form.Link
	offer.StatusId = models.OFFER_ACTIVE
	models.GetOrm().Update(offer)
	this.showSuccess(response{
		"item": offer,
	})
}

type ApiCoupons struct {
	ApiController
}

func (this *ApiCoupons) Get() {
	coupons, err := searchCoupons(this)
	if err == nil {
		this.Data["Coupons"] = coupons
		this.TplNames = "coupons.html"
		content, _ := this.RenderString()
		this.showSuccess(response {
			"content": content,
		})
	}
}
