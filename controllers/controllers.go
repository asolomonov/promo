package controllers

import (
	"github.com/byorty/beego"
	"promo/models"
	"promo/util"
	"github.com/byorty/beego/context"
	"errors"
	"github.com/byorty/beego/orm"
	"math/rand"
	"strings"
	"encoding/json"
	"sort"
	"time"
	"net/http"
	"html/template"
)

const (
	ITEMS_PER_PAGE = 20 
	REDIRECT_CODE = 301
	BAD_REQUEST = "400"
	PAGE_NOT_FOUND = "404"
)

type ExtendedController interface {
	NewPaginator(nums int64) *util.Paginator
	HasPaginator() bool
	GetPaginator() *util.Paginator
	SetData(string, interface{})
	GetBaseController() *beego.Controller
}

type MainController struct {
	beego.Controller
	User *models.User
}

func (this *MainController) Prepare() {
	this.User = this.GetSession("user").(*models.User)
}

func (this *MainController) getInput() *context.BeegoInput {
	return this.Ctx.Input
}

func (this *MainController) NewPaginator(nums int64) *util.Paginator {
	if !this.HasPaginator() {
		this.Data["Paginator"] = util.NewPaginator(this.Ctx.Request, ITEMS_PER_PAGE, nums)
	}
	return this.Data["Paginator"].(*util.Paginator)
}

func (this *MainController) HasPaginator() bool {
	_, ok := this.Data["Paginator"]
	return ok
}

func (this *MainController) GetPaginator() *util.Paginator {
	if this.HasPaginator() {
		return this.Data["Paginator"].(*util.Paginator)
	} else {
		return nil
	}
}

func (this *MainController) SetData(key string, value interface {}) {
	this.Data[key] = value
}

func (this *MainController) GetBaseController() *beego.Controller {
	return &this.Controller
}

type HtmlController struct {
	MainController
	href *util.Href
}

func (this *HtmlController) Prepare() {
	this.href = util.NewHref(this.Ctx)
	this.MainController.Prepare()
	this.Data["Title"] = beego.AppConfig.String("projectname")
	this.Data["IsProduction"] = beego.AppConfig.String("runmode") == "prod"
	this.Data["User"] = this.User
	this.Data["ProjectName"] = beego.AppConfig.String("projectname")
	if util.IS_PRODUCTION {
		this.Data["Href"] = this.href.SetHost(beego.AppConfig.String("httphost"))
	} else {
		this.Data["Href"] = this.href
	}
	this.Data["SessionExpires"] = 86400
	this.Data["AngularClient"] = this.User.GetAngularClientHash()
	if this.User.IsLoggedAdmin() {
		this.Data["CssFilename"] = "admin"
	} else {
		this.Data["CssFilename"] = "main"
	}
}

func PageNotFoundHandler(rw http.ResponseWriter, r *http.Request) {
	rw.Header().Set("Location", util.URL_ROOT)
	rw.WriteHeader(REDIRECT_CODE)
	t,_:= template.ParseFiles(beego.ViewsPath+"/empty.html")
	data :=make(map[string]interface{})
	data["content"] = "page not found"
	t.Execute(rw, data)
}

type pathParam struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

type couponSearchForm struct {
	Query    string `form:"query"`
	Status   int    `form:"status"`
	Type     int    `form:"type"`
	Order    int    `form:"order"`
}

func searchCoupons(controller ExtendedController) ([]*models.Coupon, error) {
	baseController := controller.GetBaseController()
	var count int64
	var coupons []*models.Coupon
	var pathParams pathParam
	var orderFields []string
	form := new(couponSearchForm)
	baseController.ParseForm(form)
	cond := orm.NewCondition()
	if len(form.Query) > 0 {
		form.Query = util.ClearString(form.Query)
		queryCond := orm.NewCondition()
		for _, query := range strings.Split(form.Query, util.WHITE_SPACE) {
			queryCond = queryCond.Or("Name__icontains", query)
			queryCond = queryCond.Or("Description__icontains", query)
			queryCond = queryCond.Or("Offer__name__icontains", query)
		}
		cond = cond.AndCond(queryCond)
	}
	category := baseController.Ctx.Input.Param(":category")
	if len(category) > 0 {
		selectedCategory := models.NewOfferCategory().ByPath(category)
		if selectedCategory.Id == models.INVALID_ID {
			return nil, errors.New("invalid category")
		} else {
			var offers models.Offers
			models.GetOrm().LoadRelated(selectedCategory, "Offers")
			offers = selectedCategory.Offers

			ids := make([]int, 0)
			for _, offer := range offers {
				ids = append(ids, offer.Id)
			}
			if len(ids) == 0 {
				return nil, errors.New("invalid category")
			} else {
				sort.Sort(offers)
				pathParams.Key = "category"
				pathParams.Value = category
				cond = cond.And("Offer__in", ids)
				controller.SetData("SelectedCategory", selectedCategory)
				controller.SetData("Offers", offers)
			}
		}
	}
	offer := baseController.Ctx.Input.Param(":shop")
	if len(offer) > 0 {
		selectedOffer := models.NewOffer().ByPath(offer)
		if selectedOffer.Id == models.INVALID_ID ||
			(selectedOffer.Id != models.INVALID_ID && selectedOffer.StatusId != models.OFFER_ACTIVE) {
			return nil, errors.New("invalid offer")
		} else {
			pathParams.Key = "shop"
			pathParams.Value = offer
			cond = cond.And("Offer", selectedOffer.Id)
			controller.SetData("SelectedOffer", selectedOffer)
		}
	}
	if form.Status == models.INVALID_ID {
		statusCond := orm.NewCondition()
		statusCond = statusCond.And("ExternalStatusId", models.COUPON_EXTERNAL_ANNOUNCE)
		statusCond = statusCond.Or("ExternalStatusId", models.COUPON_EXTERNAL_ACTIVE)
		statusCond = statusCond.Or("ExternalStatusId", models.COUPON_EXTERNAL_EXPIRING)
		cond = cond.AndCond(statusCond)
	} else {
		selectedStatus := models.NewCouponExternalStatus(form.Status)
		if selectedStatus.Id == models.INVALID_ID {
			return nil, errors.New("invalid status")
		} else {
			cond = cond.And("ExternalStatusId", form.Status)
			controller.SetData("SelectedStatus", selectedStatus)
		}
	}
	if form.Type != models.INVALID_ID {
		selectedType := models.NewCouponType(form.Type)
		if selectedType.Id == models.INVALID_ID {
			return nil, errors.New("invalid type")
		} else {
			cond = cond.And("TypeId", form.Type)
			controller.SetData("SelectedType", selectedType)
		}
	}
	if form.Order == models.INVALID_ID {
		orderFields = models.CouponSortRatingDesc().GetFields()
	} else {
		selectedOrder := models.NewCouponSort(form.Order)
		if selectedOrder.Id == models.INVALID_ID {
			return nil, errors.New("invalid order")
		} else {
			orderFields = selectedOrder.GetFields()
			controller.SetData("SelectedOrder", selectedOrder)
		}
	}
	cond = cond.And("StatusId", models.COUPON_ACTIVE)
	cond = cond.And("EndDate__gte", time.Now())
	count, _ = models.GetQuery(models.NewCoupon()).SetCond(cond).Count()
	paginator := controller.NewPaginator(count)
	models.GetQuery(models.NewCoupon()).
		SetCond(cond).
		OrderBy(orderFields...).
		Limit(paginator.PerPageNums).
		Offset(paginator.Offset()).
		RelatedSel().
		All(&coupons)
	if len(coupons) == 0 {
		return nil, errors.New("coupons not found")
	} else {
		paramsObject, _ := json.Marshal(pathParams)
		controller.SetData("PathParams", string(paramsObject))
		controller.SetData("Form", form)
		controller.SetData("Ratings", []int{1, 2, 3, 4, 5})
		randomCoupons(&coupons)
		return coupons, nil
	}
}

func randomCoupons(couponsPtr *[]*models.Coupon) {
	coupons := *couponsPtr
	for i := 0; i < len(coupons); i++ {
		j := rand.Intn(i + 1)
		coupons[i], coupons[j] = coupons[j], coupons[i]
	}
}
