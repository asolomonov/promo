package controllers

import (
	"promo/models"
	"github.com/byorty/beego/validation"
	"promo/util"
	"github.com/byorty/beego"
	"strconv"
)

type HomeController struct {
	HtmlController
}

func (this *HomeController) Get() {
	coupons, err := searchCoupons(this)
	if err == nil {
		goto showSuccess
	} else {
		beego.Critical(err)
		goto showError
	}

showError:
	this.initFilters()
	this.TplNames = "error/coupons.html"
	return

showSuccess:
	this.initFilters()
	this.Data["Coupons"] = coupons
	this.TplNames = "home.html"
	return
}

func (this *HomeController) initFilters() {
	this.Data["Categories"] = models.NewOfferCategory().AllActive()
	if _, ok := this.Data["Offers"]; !ok {
		this.Data["Offers"] = models.NewOffer().AllActive()
	}
	this.Data["Statuses"] = models.CouponExternalStatusActive().GetHeaderList()
	this.Data["Types"] = models.CouponTypeOther().GetList()
	this.Data["Orders"] = models.CouponSortRatingDesc().GetList()
}

type SigninForm struct {
	Email    string `form:"email" valid:"Required;Email"`
	Password string `form:"password" valid:"Required;MaxSize(10)"`
}

type SigninController struct {
	HtmlController
}

func (this *SigninController) Get() {
	this.TplNames = "signin.html"
}

func (this *SigninController) Post() {
	form := new(SigninForm)
	this.ParseForm(form)
	valid := validation.Validation{}
	ok, _ := valid.Valid(form)
	if ok {
		user := models.NewUser().ByEmail(form.Email)
		if user.Id == models.INVALID_ID {
			goto showError
		} else {
			if user.Password == util.GetHashedPassword(form.Password) {
				input := this.getInput()
				user.LoginIp = input.IP()
				user.UserAgent = input.UserAgent()
				models.GetOrm().Update(user)
				this.SetSession("user", user)
				goto showSuccess
			} else {
				goto showError
			}
		}
	} else {
		goto showError
	}

showError:
	this.Redirect(this.href.Signin().String(), 302)
	return

showSuccess:
	this.Redirect(this.href.AdminDashboard().String(), 302)
	return
}

type SignoutController struct {
	HtmlController
}

func (this *SignoutController) Get() {
	this.DelSession("user")
	this.Redirect(this.href.Root().String(), 302)
}

type CouponController struct {
	HtmlController
}

func (this *CouponController) Get() {
	var coupon *models.Coupon
	id, err := strconv.Atoi(this.Ctx.Input.Param(":id"))
	if err == nil {
		coupon = models.NewCoupon().ById(id)
		if coupon.Id == models.INVALID_ID {
			goto showError
		} else {
			goto showSuccess
		}
	} else {
		goto showError
	}

showError:
	this.TplNames = "error/coupon.html"
	return
showSuccess:
	models.GetOrm().Read(coupon.Offer)
	this.Data["Coupon"] = coupon
	this.Data["Offer"] = coupon.Offer
	this.TplNames = "coupon/iframe.html"
	return
}
