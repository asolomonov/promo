package controllers

import "promo/util"

type AdminController struct {
	HtmlController
}

func (this *AdminController) Get() {
	this.Data["AdminHeaderRouters"] = util.GetAdminHeaderRouters(this.Ctx)
	this.Data["AdminRouters"] = util.GetAdminRouters(this.Ctx)
	this.TplNames = "admin/dashboard.html"
}
