'use strict';

module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        // Metadata.
        pkg: grunt.file.readJSON('package.json'),
        // Task configuration.
        ngtemplates: {
            promo: {
                src: 'static/tpls/**/*.html',
                dest: 'static/dist/template.js'
            }
        },
        concat: {
            libs: {
                src: [
                    'static/js/libs/angular.js',
                    'static/js/libs/angular-route.js',
                    'static/js/libs/angular-resource.js',
                    'static/js/libs/angular-animate.js',
                    'static/js/libs/ui-bootstrap.js',
                ],
                dest: 'static/dist/libs.js'
            },
            app: {
                src: [
                    'static/js/*.js',
                    'static/dist/template.js'
                ],
                dest: 'static/dist/app.js'
            }
        },
        uglify: {
            options: {
                mangle: true,
                compress: {
                    sequences     : true,  // join consecutive statemets with the “comma operator”
                    properties    : true,  // optimize property access: a["foo"] → a.foo
                    dead_code     : true,  // discard unreachable code
                    drop_debugger : true,  // discard “debugger” statements
                    unsafe        : false, // some unsafe optimizations (see below)
                    conditionals  : true,  // optimize if-s and conditional expressions
                    comparisons   : true,  // optimize comparisons
                    evaluate      : true,  // evaluate constant expressions
                    booleans      : true,  // optimize boolean expressions
                    loops         : true,  // optimize loops
                    unused        : true,  // drop unused variables/functions
                    hoist_funs    : true,  // hoist function declarations
                    hoist_vars    : false, // hoist variable declarations
                    if_return     : true,  // optimize if-s followed by return/continue
                    join_vars     : true,  // join var declarations
                    cascade       : true,  // try to cascade `right` into `left` in sequences
                    side_effects  : true,  // drop side-effect-free statements
                    warnings      : true   // warn about potentially dangerous optimizations/code
                }
            },
            dist: {
                files: [{
                    expand: true,
                    src: 'static/dist/**/*.js'
                }]
            }
        },
        less: {
            dist: {
                options: {
                    compress: true,
                    cleancss: true
                },
                files: {
                    'static/dist/main.css': 'static/css/main.less'
                }
            }
        },
        clean: {
            dist: {
                src: [
                    'static/dist/template.js',
                ]
            }
        },
        'file-creator': {
            dist: {
                'conf/app.conf': function(fs, fd, done) {
                    var strs = [
                        'appname = promo',
                        'httphost = couponhub.ru',
                        'httpport = 8080',
                        '//runmode = dev',
                        'runmode = prod',
                        'sessionon = true',
                        'projectname = CouponHub',
                        'admin.email = byorty@mail.ru',
                        'postgres.default = postgres://byorty:MK99rc@localhost:5432/promo?sslmode=disable&client_encoding=utf-8',
                        'postgres.default.maxIdleConns = 100',
                        'postgres.default.maxOpenConns = 100',
                        'password.salt = 01E2if0x3ah4sh5e1a6ec72o8h9',
                        'password.charset = ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890',
                        'ap.xml = http://actionpay.ru/ru/couponFeed/id:2617;act:xmlFilter',
                    ];
                    fs.writeSync(fd, strs.join('\n'));
                    done();
                }
            }
        }
    });

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-angular-templates');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-file-creator');

    // Default task.
    grunt.registerTask(
        'default',
        [
            'ngtemplates',
            'concat',
            'uglify',
            'less',
            'clean',
        ]
    );

    grunt.registerTask(
        'createConfig',
        [
            'file-creator',
        ]
    );

};
